from auth import auth_bp
from collection_logic import collection_logic_bp


def register_all(app):
    app.register_blueprint(auth_bp)
    app.register_blueprint(collection_logic_bp)
