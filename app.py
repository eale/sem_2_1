from flask import Flask
from database import db, migrate
from config import Configuration
from utils import special
from login_manager import login_manager

app = Flask(__name__)
app.config.from_object(Configuration)
migrate.init_app(app, db)
special.register_all(app)

db.init_app(app)
login_manager.init_app(app)

if __name__ == '__main__':
    app.run()
