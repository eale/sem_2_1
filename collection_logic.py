from flask import Blueprint, render_template, redirect, url_for, flash, session, jsonify, request
import models

collection_logic_bp = Blueprint('collection_logic_bp', __name__)


@collection_logic_bp.route('/profile/add_collection', methods=['GET', 'POST'])
def add_collection():
    return render_template('add_collection.html')


@collection_logic_bp.route('/api/get_collection_items/<collection_id>')
def get_collection_items(collection_id: int):
    collection_lst = []
    items = models.CollectionItem.query.filter_by(collection_id=collection_id)
    for item in items:
        collection_item = models.Item.query.filter_by(id=item.item_id).first()
        collection_lst.append(collection_item.name)

    return jsonify(collection_lst)


@collection_logic_bp.route('/profile/<profile_id>')
def profile(profile_id: int):
    user = models.User.query.filter_by(id=profile_id).first()
    if user.private:
        return jsonify({})
    user_info = [user.email, user.name]
    user_collections = models.UserCollection.query.filter_by(user_id=profile_id)
    user_items = models.UserItem.query.filter_by(user_id=profile_id)

    result_collections = {}

    for item in user_items:
        item_obj = models.Item.query.filter_by(id=item.item_id).first()
        item_name = item_obj.name

        item_collection = models.CollectionItem.query.filter_by(item_id=item.item_id).first()
        collection_name = models.Collection.query.filter_by(id=item_collection.collection_id).first().name

        if collection_name not in result_collections.keys():
            result_collections[collection_name] = []

        result_collections[collection_name].append(item_name)

    result = [user_info, result_collections]

    return jsonify(result)





