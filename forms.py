from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo


class RegistrationForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(), Email()])
    name = StringField('name', validators=[DataRequired(), Length(min=3, max=50)])
    password = PasswordField('password', validators=[DataRequired(), Length(min=5, max=30)])
    submit = SubmitField('Регистрация')


class LoginForm(FlaskForm):
    email = StringField('User', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Login')


class ChangeForm(FlaskForm):
    new_email = StringField('email', validators=[DataRequired(), Email()])
    new_name = StringField('name', validators=[DataRequired(), Length(min=3, max=50)])
    password = PasswordField('password', validators=[DataRequired(), Length(min=5, max=30)])
    new_password = PasswordField('password', validators=[DataRequired(), Length(min=5, max=30)])
    submit = SubmitField('Подтвердить')
