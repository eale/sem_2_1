from flask import Blueprint, render_template, redirect, url_for, flash, session
from flask_login import login_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

from database import db
from forms import RegistrationForm, LoginForm, ChangeForm
from login_manager import login_manager
from models import User

auth_bp = Blueprint('auth_bp', __name__)


@auth_bp.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegistrationForm()
    print(form.data)
    if form.validate_on_submit():
        user_email = User.query.filter_by(email=form.email.data).first()
        if user_email:
            flash('Почта занята')
            return redirect(url_for('auth_bp.signup'))
        user = User(email=str(form.email.data), name=str(form.name.data),
                    password=generate_password_hash(str(form.password.data), method='sha256'))
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('auth_bp.login'))
    return render_template('signup.html', form=form)


@auth_bp.route('/login/', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('auth_bp.profile'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data)[0]
        if user and user.check_password(form.password.data):
            login_user(user, remember=form.remember_me.data)
            return redirect(url_for('auth_bp.profile'))
        return redirect(url_for('auth_bp.login'))

    return render_template('login.html', form=form)


@auth_bp.route('/profile/', methods=['GET', 'POST'])
def profile():
    if current_user.is_authenticated:
        user_id = session['_user_id']
        user = User.query.get(user_id)
        user_info = [user.email, user.name]
        return render_template('profile.html', user_info=user_info)


@auth_bp.route('/profile/change/', methods=['GET', 'POST'])
def change_profile():
    form = ChangeForm()
    if form.validate_on_submit():
        user_id = session['_user_id']
        user = User.query.get(user_id)
        if check_password_hash(user.password, str(form.password.data)):
            user.name = str(form.new_name.data)
            user.password = generate_password_hash(str(form.password.data), method='sha256')
            user.email = str(form.new_email.data)
            db.session.commit()
            return redirect(url_for('auth_bp.profile'))
        return redirect(url_for('auth_bp.change_profile'))
    return render_template('change_profile.html', form=form)


@auth_bp.route('/logout', methods=('GET', 'POST'))
def logout():
    session.clear()
    return redirect(url_for('auth_bp.main_page'))


@auth_bp.route('/', methods=('GET',))
def main_page():
    return render_template('base.html')


@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return User.query.get(int(user_id))
    return None


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('auth_bp.login'))
