from database import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    private = db.Column(db.Boolean, default=False)
    name = db.Column(db.VARCHAR(50), nullable=False, unique=True)
    email = db.Column(db.VARCHAR(100), nullable=False, unique=True)
    password = db.Column(db.VARCHAR, nullable=False)


